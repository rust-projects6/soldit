use anchor_lang::prelude::*;

declare_id!("2Z4x9pjJLHf7hZrtFmFPoQoQjnbiGrBoGnrfgiQ8Z6zv");

#[program]
pub mod soldit {
    use super::*;
    use solana_program::entrypoint::ProgramResult;
    pub fn create_user(ctx: Context<CreateUser>, username: String) -> Result<()> {
        let user = &mut ctx.accounts.user;
        user.authority = ctx.accounts.authority.key();
        user.username = username;
        Ok(())
    }

    pub fn create_feed(ctx: Context<CreateFeed>) -> Result<()> {
        let feed = &mut ctx.accounts.feed;
        feed.thread_count = 0;
        Ok(())
    }

    pub fn create_thread(ctx: Context<CreateThread>, title: String, content: String) -> Result<()> {
        let feed = &mut ctx.accounts.feed;
        feed.thread_count += 1;

        let user = &mut ctx.accounts.user;

        let thread = &mut ctx.accounts.thread;
        let clock: Clock = Clock::get()?;

        thread.title = title;
        thread.content = content;
        thread.authority = ctx.accounts.authority.key();
        thread.owner_username = user.username.clone();
        thread.timestamp = clock.unix_timestamp;
        thread.vote = 0;
        Ok(())
    }

    pub fn create_thread_upvote(
        ctx: Context<CreateThreadUpVote>,
        thread_id: u64,
        is_up: bool,
    ) -> Result<()> {
        (*ctx.accounts.thread).vote += if is_up { 1 } else { -1 };

        let thread_vote = &mut ctx.accounts.thread_vote;
        thread_vote.thread_id = thread_id;
        thread_vote.authority = ctx.accounts.authority.key();
        thread_vote.vote = if is_up { 1 } else { -1 };
        Ok(())
    }

    pub fn create_comment(
        ctx: Context<CreateComment>,
        _thread_id: u64,
        content: String,
        is_root: bool,
        parent_id: u64,
    ) -> ProgramResult {
        (*ctx.accounts.thread).comment_count += 1;

        let comment = &mut ctx.accounts.comment;
        comment.is_root = is_root;
        comment.vote = 0;
        comment.parent_comment = parent_id;
        comment.content = content;
        comment.authority = ctx.accounts.authority.key();
        Ok(())
    }

    pub fn crate_comment_vote(
        ctx: Context<CreateCommentVote>,
        _thread_id: u64,
        _comment_id: u64,
        is_up: bool,
    ) -> Result<()> {
        let comment = &mut ctx.accounts.comment;
        comment.vote += if is_up { 1 } else { -1 };

        let comment_vote = &mut ctx.accounts.comment_vote;
        comment_vote.authority = ctx.accounts.authority.key();
        comment_vote.vote = if is_up { 1 } else { -1 };
        Ok(())
    }
}

#[derive(Accounts)]
pub struct CreateUser<'info> {
    #[account(init, payer= authority,
        seeds = [b"user".as_ref(), authority.key().as_ref()],
        bump,
        space = 8 + UserAccount::INIT_SPACE)]
    pub user: Account<'info, UserAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct CreateFeed<'info> {
    #[account(init, payer = authority, seeds=[b"feed".as_ref()], bump, space = 8 + FeedAccount::INIT_SPACE)]
    pub feed: Account<'info, FeedAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct CreateThread<'info> {
    #[account(mut,
        seeds = [b"user".as_ref(), authority.key().as_ref()],
        bump)]
    pub user: Account<'info, UserAccount>,

    #[account(mut)]
    feed: Account<'info, FeedAccount>,

    #[account(init, payer = authority, seeds=[b"thread".as_ref(), feed.thread_count.to_be_bytes().as_ref()], bump, space = 8 + ThreadAccount::INIT_SPACE)]
    pub thread: Account<'info, ThreadAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
    // pub clock: Sysvar<'info, Clock>,
}

#[derive(Accounts)]
#[instruction(thread_id: u64)]
pub struct CreateThreadUpVote<'info> {
    #[account(mut, seeds=[b"thread".as_ref(), thread_id.to_be_bytes().as_ref()], bump)]
    pub thread: Account<'info, ThreadAccount>,

    #[account(init,
        payer= authority,
        seeds=[b"thread_vote".as_ref(), authority.key().as_ref(), thread_id.to_be_bytes().as_ref()],
        bump,
        space = 8 + ThreadUpvoteAccount::INIT_SPACE)]
    pub thread_vote: Account<'info, ThreadUpvoteAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(thread_id: u64)]
pub struct CreateComment<'info> {
    #[account(mut, seeds = [b"thread".as_ref(), thread_id.to_be_bytes().as_ref()], bump)]
    pub thread: Account<'info, ThreadAccount>,

    #[account(init,
        payer = authority,
        seeds = [b"comment".as_ref(), thread_id.to_be_bytes().as_ref(), thread.comment_count.to_be_bytes().as_ref()],
        bump,
        space = 8+CommentAccount::INIT_SPACE)]
    pub comment: Account<'info, CommentAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(thread_id:u64, comment_id: u64)]
pub struct CreateCommentVote<'info> {
    #[account(mut, seeds=[b"comment".as_ref(), thread_id.to_be_bytes().as_ref(), comment_id.to_be_bytes().as_ref()], bump)]
    pub comment: Account<'info, CommentAccount>,

    #[account(init, payer=authority,
        seeds=[b"comment_vote".as_ref(),thread_id.to_be_bytes().as_ref(), comment_id.to_be_bytes().as_ref(), authority.key().as_ref()],
        bump,
        space = 8 + CommentVoteAccount::INIT_SPACE )]
    pub comment_vote: Account<'info, CommentVoteAccount>,

    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[account]
#[derive(InitSpace)]
pub struct UserAccount {
    pub authority: Pubkey,
    #[max_len(100)]
    pub username: String,
}

#[account]
#[derive(InitSpace)]
pub struct FeedAccount {
    pub thread_count: u64,
}

#[account]
#[derive(InitSpace)]
pub struct ThreadAccount {
    pub authority: Pubkey,
    #[max_len(200)]
    pub title: String,
    #[max_len(400)]
    pub content: String,
    pub timestamp: i64,
    pub vote: i64,
    pub comment_count: u64,
    #[max_len(100)]
    pub owner_username: String,
}

#[account]
#[derive(InitSpace)]
pub struct ThreadUpvoteAccount {
    pub authority: Pubkey,
    pub vote: i8,
    pub thread_id: u64,
}

#[account]
#[derive(InitSpace)]
pub struct CommentAccount {
    pub authority: Pubkey,
    #[max_len(200)]
    pub content: String,
    pub is_root: bool,
    pub parent_comment: u64,
    pub vote: i8,
}

#[account]
#[derive(InitSpace)]
pub struct CommentVoteAccount {
    pub authority: Pubkey,
    pub vote: i8,
}
