import * as anchor from "@coral-xyz/anchor";
import { BN, Program } from "@coral-xyz/anchor";
import { utf8 } from "@coral-xyz/anchor/dist/cjs/utils/bytes";
import { assert } from "chai";
import { Soldit } from "../target/types/soldit";

describe("soldit", () => {
  const provider = anchor.AnchorProvider.env();
  anchor.setProvider(provider);

  const program = anchor.workspace.Soldit as Program<Soldit>;
  const publicKey = anchor.AnchorProvider.local().wallet.publicKey; // provider.wallet.publicKey;
  const USERNAME = "hiron";

  it("Creat a user", async () => {
    const userPDA = await getUserPDA();
    await program.methods
      .createUser(USERNAME)
      .accounts({
        user: userPDA,
        authority: publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .rpc();

    const userAccount = await program.account.userAccount.fetch(userPDA);
    console.log(userAccount);
    assert.equal(userAccount.username.toString(), USERNAME);
    assert.equal(userAccount.authority.toString(), publicKey.toString());
  });

  it("Create feed Account", async () => {
    const feedPda = await getFeedPDA();

    await program.methods
      .createFeed()
      .accounts({
        feed: feedPda,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .rpc();

    const feedAccount = await program.account.feedAccount.fetch(feedPda);
    console.log(feedAccount);
    assert.equal(feedAccount.threadCount.toNumber(), 0);
  });

  it("Create thread Account", async () => {
    const userPDA = await getUserPDA();
    const feedPDA = await getFeedPDA();
    let feedAccount = await program.account.feedAccount.fetch(feedPDA);
    const threadPDA = await getThreadPDA(feedAccount.threadCount.toNumber());

    await program.methods
      .createThread("this is the title", "this is the content")
      .accounts({
        user: userPDA,
        feed: feedPDA,
        authority: publicKey,
        thread: threadPDA,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .rpc();

    const threadAccount = await program.account.threadAccount.fetch(threadPDA);
    console.log(threadAccount);
    assert.equal(threadAccount.title.toString(), "this is the title");
  });

  it("Create thread Upvote account", async () => {
    const feedPDA = await getFeedPDA();
    const feedAccount = await program.account.feedAccount.fetch(feedPDA);
    let threadCount = feedAccount.threadCount.toNumber() - 1;

    const upvotePDA = await getUpvotePDA(threadCount, publicKey);
    const threadPDA = await getThreadPDA(threadCount);

    await program.methods
      .createThreadUpvote(new BN(threadCount), true)
      .accounts({
        thread: threadPDA,
        threadVote: upvotePDA,
        authority: publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .rpc();

    const threadVoat = await program.account.threadUpvoteAccount.fetch(
      upvotePDA
    );
    console.log(threadVoat);

    assert.equal(threadVoat.vote, 1);
    console.log(feedAccount);
  });

  it("Create comment Account", async () => {
    const feedPDA = await getFeedPDA();
    const feedAccount = await program.account.feedAccount.fetch(feedPDA);
    let threadCount = feedAccount.threadCount.toNumber() - 1;

    const threadPDA = await getThreadPDA(threadCount);
    const threadAccount = await program.account.threadAccount.fetch(threadPDA);
    let commentCount = threadAccount.commentCount.toNumber();

    console.log(threadAccount);

    const commentPDA = await getCommentPDA(threadCount, commentCount);

    await program.methods
      .createComment(
        new BN(threadCount),
        "This is a comment!!",
        false,
        new BN(0)
      )
      .accounts({
        thread: threadPDA,
        comment: commentPDA,
        authority: publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .rpc();

    const commentAccount = await program.account.commentAccount.fetch(
      commentPDA
    );
    console.log(commentAccount);

    assert.equal(commentAccount.vote.valueOf(), 0);
  });

  it("Create comment account Up vote", async () => {
    const feedPDA = await getFeedPDA();
    const feedAccount = await program.account.feedAccount.fetch(feedPDA);
    let threadId = feedAccount.threadCount.toNumber() - 1;

    const threadPDA = await getThreadPDA(threadId);
    const threadAccount = await program.account.threadAccount.fetch(threadPDA);
    let commentId = threadAccount.commentCount.toNumber() - 1;

    const commentPDA = await getCommentPDA(threadId, commentId);

    const commentVotePDA = await getCommentVotePDA(
      threadId,
      commentId,
      publicKey
    );

    await program.methods
      .crateCommentVote(new BN(threadId), new BN(commentId), true)
      .accounts({
        comment: commentPDA,
        commentVote: commentVotePDA,
        authority: publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .rpc();

    const commentAccount = await program.account.commentAccount.fetch(
      commentPDA
    );
    console.log(commentAccount);
    assert.equal(commentAccount.vote.valueOf(), 1);

    const commentVoteAccount = await program.account.commentVoteAccount.fetch(
      commentVotePDA
    );
    console.log(commentVoteAccount);
    assert.equal(commentVoteAccount.authority.toString(), publicKey.toString());
    assert.equal(commentVoteAccount.vote.valueOf(), 1);
  });

  async function getUserPDA(): Promise<anchor.web3.PublicKey> {
    const [userPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("user"), publicKey.toBuffer()],
      program.programId
    );

    return userPDA;
  }

  async function getFeedPDA(): Promise<anchor.web3.PublicKey> {
    const [feedPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("feed")],
      program.programId
    );
    return feedPDA;
  }

  async function getThreadPDA(
    threadID: number
  ): Promise<anchor.web3.PublicKey> {
    const [threadPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("thread"), new BN(threadID).toBuffer("be", 8)],
      program.programId
    );
    return threadPDA;
  }

  async function getUpvotePDA(
    threadID: number,
    authorityKey: anchor.web3.PublicKey
  ): Promise<anchor.web3.PublicKey> {
    const threadIDBuffer = new BN(threadID).toBuffer("be", 8);
    const [UpvotePDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("thread_vote"), authorityKey.toBuffer(), threadIDBuffer],
      program.programId
    );
    return UpvotePDA;
  }

  async function getCommentPDA(
    threadID: number,
    commentID: number
  ): Promise<anchor.web3.PublicKey> {
    const threadIDBuffer = new BN(threadID).toBuffer("be", 8);
    const commentIDBuffer = new BN(commentID).toBuffer("be", 8);
    const [commentPDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [utf8.encode("comment"), threadIDBuffer, commentIDBuffer],
      program.programId
    );
    return commentPDA;
  }

  async function getCommentVotePDA(
    threadId: number,
    commentId: number,
    key: anchor.web3.PublicKey
  ): Promise<anchor.web3.PublicKey> {
    const threadIdBuffer = new BN(threadId).toBuffer("be", 8);
    const commentIdBuffer = new BN(commentId).toBuffer("be", 8);

    const [commentVotePDA] = anchor.web3.PublicKey.findProgramAddressSync(
      [
        utf8.encode("comment_vote"),
        threadIdBuffer,
        commentIdBuffer,
        key.toBuffer(),
      ],
      program.programId
    );

    return commentVotePDA;
  }
});
